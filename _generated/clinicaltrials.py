# -*- coding: utf-8 -*-

from elasticsearch_dsl import \
    DocType, Nested, Date, Float, Integer, Keyword, Text


class Address(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                city=Text(),
                state=Text(),
                zip=Keyword(),
                country=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('city')
        if child is not None:
            a['city'] = child.text
        child = element.find('state')
        if child is not None:
            a['state'] = child.text
        child = element.find('zip')
        if child is not None:
            a['zip'] = child.text
        child = element.find('country')
        if child is not None:
            a['country'] = child.text

        return a


class ArmGroup(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                arm_group_label=Text(),
                arm_group_type=Keyword(),
                description=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('arm_group_label')
        if child is not None:
            a['arm_group_label'] = child.text
        child = element.find('arm_group_type')
        if child is not None:
            a['arm_group_type'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text

        return a


class BiospecDescr(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                textblock=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('textblock')
        if child is not None:
            a['textblock'] = child.text

        return a


class BriefSummary(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                textblock=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('textblock')
        if child is not None:
            a['textblock'] = child.text

        return a


class CertainAgreements(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                pi_employee=Keyword(),
                restrictive_agreement=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('pi_employee')
        if child is not None:
            a['pi_employee'] = child.text
        child = element.find('restrictive_agreement')
        if child is not None:
            a['restrictive_agreement'] = child.text

        return a


class Collaborator(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                agency=Text(),
                agency_class=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('agency')
        if child is not None:
            a['agency'] = child.text
        child = element.find('agency_class')
        if child is not None:
            a['agency_class'] = child.text

        return a


class CompletionDate(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                type=Keyword(),
                inner_text=Date()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['type'] = element.attrib.get('type')
        a['inner_text'] = element.text

        return a


class ConditionBrowse(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                mesh_term=Text(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['mesh_term'] = \
            [child.text for child in element.findall('mesh_term')]

        return a


class Contact(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                last_name=Text(),
                phone=Keyword(),
                email=Text(),
                phone_ext=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('last_name')
        if child is not None:
            a['last_name'] = child.text
        child = element.find('phone')
        if child is not None:
            a['phone'] = child.text
        child = element.find('email')
        if child is not None:
            a['email'] = child.text
        child = element.find('phone_ext')
        if child is not None:
            a['phone_ext'] = child.text

        return a


class ContactBackup(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                last_name=Text(),
                phone=Keyword(),
                email=Text(),
                phone_ext=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('last_name')
        if child is not None:
            a['last_name'] = child.text
        child = element.find('phone')
        if child is not None:
            a['phone'] = child.text
        child = element.find('email')
        if child is not None:
            a['email'] = child.text
        child = element.find('phone_ext')
        if child is not None:
            a['phone_ext'] = child.text

        return a


class Count(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id=Keyword(),
                value=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group_id'] = element.attrib.get('group_id')
        a['value'] = element.attrib.get('value')

        return a


class CountList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                count=Count(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['count'] = \
            [Count.from_element(child) for child in element.findall('count')]

        return a


class Counts(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id=Keyword(),
                subjects_affected=Integer(),
                subjects_at_risk=Integer(),
                events=Integer(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group_id'] = element.attrib.get('group_id')
        a['subjects_affected'] = element.attrib.get('subjects_affected')
        a['subjects_at_risk'] = element.attrib.get('subjects_at_risk')
        a['events'] = element.attrib.get('events')

        return a


class Criteria(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                textblock=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('textblock')
        if child is not None:
            a['textblock'] = child.text

        return a


class DetailedDescription(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                textblock=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('textblock')
        if child is not None:
            a['textblock'] = child.text

        return a


class Enrollment(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                type=Keyword(),
                inner_text=Integer()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['type'] = element.attrib.get('type')
        a['inner_text'] = element.text

        return a


class ExpandedAccessInfo(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                expanded_access_type_individual=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('expanded_access_type_individual')
        if child is not None:
            a['expanded_access_type_individual'] = child.text

        return a


class Facility(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                facility_name=Text(),
                address=Address(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('name')
        if child is not None:
            a['facility_name'] = child.text
        child = element.find('address')
        if child is not None:
            a['address'] = Address.from_element(child)

        return a


class Group(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id=Keyword(),
                title=Text(),
                description=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group_id'] = element.attrib.get('group_id')
        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text

        return a


class GroupIdList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id=Keyword(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group_id'] = \
            [child.text for child in element.findall('group_id')]

        return a


class GroupList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group=Group(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group'] = \
            [Group.from_element(child) for child in element.findall('group')]

        return a


class IdInfo(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                org_study_id=Keyword(),
                secondary_id=Keyword(multi=True),
                nct_id=Keyword(),
                nct_alias=Keyword(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('org_study_id')
        if child is not None:
            a['org_study_id'] = child.text
        a['secondary_id'] = \
            [child.text for child in element.findall('secondary_id')]
        child = element.find('nct_id')
        if child is not None:
            a['nct_id'] = child.text
        a['nct_alias'] = \
            [child.text for child in element.findall('nct_alias')]

        return a


class Intervention(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                intervention_type=Keyword(),
                intervention_name=Text(),
                description=Text(),
                arm_group_label=Text(multi=True),
                other_name=Text(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('intervention_type')
        if child is not None:
            a['intervention_type'] = child.text
        child = element.find('intervention_name')
        if child is not None:
            a['intervention_name'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text
        a['arm_group_label'] = \
            [child.text for child in element.findall('arm_group_label')]
        a['other_name'] = \
            [child.text for child in element.findall('other_name')]

        return a


class InterventionBrowse(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                mesh_term=Text(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['mesh_term'] = \
            [child.text for child in element.findall('mesh_term')]

        return a


class Investigator(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                last_name=Text(),
                role=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('last_name')
        if child is not None:
            a['last_name'] = child.text
        child = element.find('role')
        if child is not None:
            a['role'] = child.text

        return a


class LeadSponsor(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                agency=Text(),
                agency_class=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('agency')
        if child is not None:
            a['agency'] = child.text
        child = element.find('agency_class')
        if child is not None:
            a['agency_class'] = child.text

        return a


class Link(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                url=Text(),
                description=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('url')
        if child is not None:
            a['url'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text

        return a


class Location(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                facility=Facility(),
                status=Keyword(),
                contact=Contact(),
                investigator=Investigator(multi=True),
                contact_backup=ContactBackup(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('facility')
        if child is not None:
            a['facility'] = Facility.from_element(child)
        child = element.find('status')
        if child is not None:
            a['status'] = child.text
        child = element.find('contact')
        if child is not None:
            a['contact'] = Contact.from_element(child)
        a['investigator'] = \
            [Investigator.from_element(child) for child in element.findall('investigator')]
        child = element.find('contact_backup')
        if child is not None:
            a['contact_backup'] = ContactBackup.from_element(child)

        return a


class LocationCountries(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                country=Keyword(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['country'] = \
            [child.text for child in element.findall('country')]

        return a


class Measurement(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id=Keyword(),
                value=Keyword(),
                spread=Keyword(),
                lower_limit=Keyword(),
                upper_limit=Keyword(),
                inner_text=Text()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group_id'] = element.attrib.get('group_id')
        a['value'] = element.attrib.get('value')
        a['spread'] = element.attrib.get('spread')
        a['lower_limit'] = element.attrib.get('lower_limit')
        a['upper_limit'] = element.attrib.get('upper_limit')
        a['inner_text'] = element.text

        return a


class MeasurementList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                measurement=Measurement(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['measurement'] = \
            [Measurement.from_element(child) for child in element.findall('measurement')]

        return a


class OtherOutcome(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                measure=Text(),
                time_frame=Text(),
                description=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('measure')
        if child is not None:
            a['measure'] = child.text
        child = element.find('time_frame')
        if child is not None:
            a['time_frame'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text

        return a


class OverallContact(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                last_name=Text(),
                phone=Keyword(),
                email=Text(),
                phone_ext=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('last_name')
        if child is not None:
            a['last_name'] = child.text
        child = element.find('phone')
        if child is not None:
            a['phone'] = child.text
        child = element.find('email')
        if child is not None:
            a['email'] = child.text
        child = element.find('phone_ext')
        if child is not None:
            a['phone_ext'] = child.text

        return a


class OverallContactBackup(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                last_name=Text(),
                phone=Keyword(),
                email=Text(),
                phone_ext=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('last_name')
        if child is not None:
            a['last_name'] = child.text
        child = element.find('phone')
        if child is not None:
            a['phone'] = child.text
        child = element.find('email')
        if child is not None:
            a['email'] = child.text
        child = element.find('phone_ext')
        if child is not None:
            a['phone_ext'] = child.text

        return a


class OverallOfficial(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                last_name=Text(),
                role=Keyword(),
                affiliation=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('last_name')
        if child is not None:
            a['last_name'] = child.text
        child = element.find('role')
        if child is not None:
            a['role'] = child.text
        child = element.find('affiliation')
        if child is not None:
            a['affiliation'] = child.text

        return a


class OversightInfo(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                has_dmc=Keyword(),
                is_fda_regulated_drug=Keyword(),
                is_fda_regulated_device=Keyword(),
                is_unapproved_device=Keyword(),
                is_ppsd=Keyword(),
                is_us_export=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('has_dmc')
        if child is not None:
            a['has_dmc'] = child.text
        child = element.find('is_fda_regulated_drug')
        if child is not None:
            a['is_fda_regulated_drug'] = child.text
        child = element.find('is_fda_regulated_device')
        if child is not None:
            a['is_fda_regulated_device'] = child.text
        child = element.find('is_unapproved_device')
        if child is not None:
            a['is_unapproved_device'] = child.text
        child = element.find('is_ppsd')
        if child is not None:
            a['is_ppsd'] = child.text
        child = element.find('is_us_export')
        if child is not None:
            a['is_us_export'] = child.text

        return a


class Participants(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id=Keyword(),
                count=Integer(),
                inner_text=Text()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['group_id'] = element.attrib.get('group_id')
        a['count'] = element.attrib.get('count')
        a['inner_text'] = element.text

        return a


class ParticipantsList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                participants=Participants(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['participants'] = \
            [Participants.from_element(child) for child in element.findall('participants')]

        return a


class PatientData(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                sharing_ipd=Keyword(),
                ipd_description=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('sharing_ipd')
        if child is not None:
            a['sharing_ipd'] = child.text
        child = element.find('ipd_description')
        if child is not None:
            a['ipd_description'] = child.text

        return a


class PointOfContact(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                name_or_title=Text(),
                organization=Text(),
                phone=Keyword(),
                email=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('name_or_title')
        if child is not None:
            a['name_or_title'] = child.text
        child = element.find('organization')
        if child is not None:
            a['organization'] = child.text
        child = element.find('phone')
        if child is not None:
            a['phone'] = child.text
        child = element.find('email')
        if child is not None:
            a['email'] = child.text

        return a


class PrimaryCompletionDate(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                type=Keyword(),
                inner_text=Date()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['type'] = element.attrib.get('type')
        a['inner_text'] = element.text

        return a


class PrimaryOutcome(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                measure=Text(),
                time_frame=Text(),
                description=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('measure')
        if child is not None:
            a['measure'] = child.text
        child = element.find('time_frame')
        if child is not None:
            a['time_frame'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text

        return a


class Reference(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                citation=Text(),
                PMID=Integer(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('citation')
        if child is not None:
            a['citation'] = child.text
        child = element.find('PMID')
        if child is not None:
            a['PMID'] = child.text

        return a


class RemovedCountries(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                country=Keyword(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['country'] = \
            [child.text for child in element.findall('country')]

        return a


class RequiredHeader(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                download_date=Keyword(),
                link_text=Keyword(),
                url=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('download_date')
        if child is not None:
            a['download_date'] = child.text
        child = element.find('link_text')
        if child is not None:
            a['link_text'] = child.text
        child = element.find('url')
        if child is not None:
            a['url'] = child.text

        return a


class ResponsibleParty(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                responsible_party_type=Keyword(),
                investigator_affiliation=Text(),
                investigator_full_name=Text(),
                investigator_title=Text(),
                name_title=Text(),
                organization=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('responsible_party_type')
        if child is not None:
            a['responsible_party_type'] = child.text
        child = element.find('investigator_affiliation')
        if child is not None:
            a['investigator_affiliation'] = child.text
        child = element.find('investigator_full_name')
        if child is not None:
            a['investigator_full_name'] = child.text
        child = element.find('investigator_title')
        if child is not None:
            a['investigator_title'] = child.text
        child = element.find('name_title')
        if child is not None:
            a['name_title'] = child.text
        child = element.find('organization')
        if child is not None:
            a['organization'] = child.text

        return a


class ResultsReference(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                citation=Text(),
                PMID=Integer(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('citation')
        if child is not None:
            a['citation'] = child.text
        child = element.find('PMID')
        if child is not None:
            a['PMID'] = child.text

        return a


class SecondaryOutcome(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                measure=Text(),
                time_frame=Text(),
                description=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('measure')
        if child is not None:
            a['measure'] = child.text
        child = element.find('time_frame')
        if child is not None:
            a['time_frame'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text

        return a


class Sponsors(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                lead_sponsor=LeadSponsor(),
                collaborator=Collaborator(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('lead_sponsor')
        if child is not None:
            a['lead_sponsor'] = LeadSponsor.from_element(child)
        a['collaborator'] = \
            [Collaborator.from_element(child) for child in element.findall('collaborator')]

        return a


class StartDate(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                type=Keyword(),
                inner_text=Date()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['type'] = element.attrib.get('type')
        a['inner_text'] = element.text

        return a


class StudyDesignInfo(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                primary_purpose=Keyword(),
                observational_model=Keyword(),
                time_perspective=Keyword(),
                allocation=Keyword(),
                intervention_model=Keyword(),
                masking=Keyword(),
                intervention_model_description=Keyword(),
                masking_description=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('primary_purpose')
        if child is not None:
            a['primary_purpose'] = child.text
        child = element.find('observational_model')
        if child is not None:
            a['observational_model'] = child.text
        child = element.find('time_perspective')
        if child is not None:
            a['time_perspective'] = child.text
        child = element.find('allocation')
        if child is not None:
            a['allocation'] = child.text
        child = element.find('intervention_model')
        if child is not None:
            a['intervention_model'] = child.text
        child = element.find('masking')
        if child is not None:
            a['masking'] = child.text
        child = element.find('intervention_model_description')
        if child is not None:
            a['intervention_model_description'] = child.text
        child = element.find('masking_description')
        if child is not None:
            a['masking_description'] = child.text

        return a


class StudyDoc(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                doc_type=Keyword(),
                doc_url=Keyword(),
                doc_comment=Keyword(),
                doc_id=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('doc_type')
        if child is not None:
            a['doc_type'] = child.text
        child = element.find('doc_url')
        if child is not None:
            a['doc_url'] = child.text
        child = element.find('doc_comment')
        if child is not None:
            a['doc_comment'] = child.text
        child = element.find('doc_id')
        if child is not None:
            a['doc_id'] = child.text

        return a


class StudyDocs(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                study_doc=StudyDoc(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('study_doc')
        if child is not None:
            a['study_doc'] = StudyDoc.from_element(child)

        return a


class StudyPop(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                textblock=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('textblock')
        if child is not None:
            a['textblock'] = child.text

        return a


class SubTitle(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                vocab=Keyword(),
                inner_text=Text()
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['vocab'] = element.attrib.get('vocab')
        a['inner_text'] = element.text

        return a


class Analysis(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_id_list=GroupIdList(),
                non_inferiority_type=Keyword(),
                param_type=Keyword(),
                param_value=Float(),
                ci_percent=Float(),
                ci_n_sides=Keyword(),
                ci_lower_limit=Float(),
                ci_upper_limit=Float(),
                groups_desc=Text(),
                p_value=Keyword(),
                method=Keyword(),
                estimate_desc=Text(),
                non_inferiority_desc=Keyword(),
                p_value_desc=Keyword(),
                method_desc=Keyword(),
                dispersion_type=Keyword(),
                dispersion_value=Float(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('group_id_list')
        if child is not None:
            a['group_id_list'] = GroupIdList.from_element(child)
        child = element.find('non_inferiority_type')
        if child is not None:
            a['non_inferiority_type'] = child.text
        child = element.find('param_type')
        if child is not None:
            a['param_type'] = child.text
        child = element.find('param_value')
        if child is not None:
            a['param_value'] = child.text
        child = element.find('ci_percent')
        if child is not None:
            a['ci_percent'] = child.text
        child = element.find('ci_n_sides')
        if child is not None:
            a['ci_n_sides'] = child.text
        child = element.find('ci_lower_limit')
        if child is not None:
            a['ci_lower_limit'] = child.text
        child = element.find('ci_upper_limit')
        if child is not None:
            a['ci_upper_limit'] = child.text
        child = element.find('groups_desc')
        if child is not None:
            a['groups_desc'] = child.text
        child = element.find('p_value')
        if child is not None:
            a['p_value'] = child.text
        child = element.find('method')
        if child is not None:
            a['method'] = child.text
        child = element.find('estimate_desc')
        if child is not None:
            a['estimate_desc'] = child.text
        child = element.find('non_inferiority_desc')
        if child is not None:
            a['non_inferiority_desc'] = child.text
        child = element.find('p_value_desc')
        if child is not None:
            a['p_value_desc'] = child.text
        child = element.find('method_desc')
        if child is not None:
            a['method_desc'] = child.text
        child = element.find('dispersion_type')
        if child is not None:
            a['dispersion_type'] = child.text
        child = element.find('dispersion_value')
        if child is not None:
            a['dispersion_value'] = child.text

        return a


class AnalysisList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                analysis=Analysis(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['analysis'] = \
            [Analysis.from_element(child) for child in element.findall('analysis')]

        return a


class Analyzed(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                units=Keyword(),
                scope=Keyword(),
                count_list=CountList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('units')
        if child is not None:
            a['units'] = child.text
        child = element.find('scope')
        if child is not None:
            a['scope'] = child.text
        child = element.find('count_list')
        if child is not None:
            a['count_list'] = CountList.from_element(child)

        return a


class AnalyzedList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                analyzed=Analyzed(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['analyzed'] = \
            [Analyzed.from_element(child) for child in element.findall('analyzed')]

        return a


class DropWithdrawReason(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                title=Text(),
                participants_list=ParticipantsList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('participants_list')
        if child is not None:
            a['participants_list'] = ParticipantsList.from_element(child)

        return a


class DropWithdrawReasonList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                drop_withdraw_reason=DropWithdrawReason(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['drop_withdraw_reason'] = \
            [DropWithdrawReason.from_element(child) for child in element.findall('drop_withdraw_reason')]

        return a


class Eligibility(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                criteria=Criteria(),
                gender=Keyword(),
                minimum_age=Keyword(),
                maximum_age=Keyword(),
                healthy_volunteers=Keyword(),
                study_pop=StudyPop(),
                sampling_method=Keyword(),
                gender_based=Keyword(),
                gender_description=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('criteria')
        if child is not None:
            a['criteria'] = Criteria.from_element(child)
        child = element.find('gender')
        if child is not None:
            a['gender'] = child.text
        child = element.find('minimum_age')
        if child is not None:
            a['minimum_age'] = child.text
        child = element.find('maximum_age')
        if child is not None:
            a['maximum_age'] = child.text
        child = element.find('healthy_volunteers')
        if child is not None:
            a['healthy_volunteers'] = child.text
        child = element.find('study_pop')
        if child is not None:
            a['study_pop'] = StudyPop.from_element(child)
        child = element.find('sampling_method')
        if child is not None:
            a['sampling_method'] = child.text
        child = element.find('gender_based')
        if child is not None:
            a['gender_based'] = child.text
        child = element.find('gender_description')
        if child is not None:
            a['gender_description'] = child.text

        return a


class Event(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                sub_title=SubTitle(),
                counts=Counts(multi=True),
                description=Text(),
                assessment=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('sub_title')
        if child is not None:
            a['sub_title'] = SubTitle.from_element(child)
        a['counts'] = \
            [Counts.from_element(child) for child in element.findall('counts')]
        child = element.find('description')
        if child is not None:
            a['description'] = child.text
        child = element.find('assessment')
        if child is not None:
            a['assessment'] = child.text

        return a


class EventList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                event=Event(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['event'] = \
            [Event.from_element(child) for child in element.findall('event')]

        return a


class Milestone(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                title=Keyword(),
                participants_list=ParticipantsList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('participants_list')
        if child is not None:
            a['participants_list'] = ParticipantsList.from_element(child)

        return a


class MilestoneList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                milestone=Milestone(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['milestone'] = \
            [Milestone.from_element(child) for child in element.findall('milestone')]

        return a


class Period(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                title=Keyword(),
                milestone_list=MilestoneList(),
                drop_withdraw_reason_list=DropWithdrawReasonList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('milestone_list')
        if child is not None:
            a['milestone_list'] = MilestoneList.from_element(child)
        child = element.find('drop_withdraw_reason_list')
        if child is not None:
            a['drop_withdraw_reason_list'] = DropWithdrawReasonList.from_element(child)

        return a


class PeriodList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                period=Period(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['period'] = \
            [Period.from_element(child) for child in element.findall('period')]

        return a


class Category(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                measurement_list=MeasurementList(),
                title=Keyword(),
                event_list=EventList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('measurement_list')
        if child is not None:
            a['measurement_list'] = MeasurementList.from_element(child)
        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('event_list')
        if child is not None:
            a['event_list'] = EventList.from_element(child)

        return a


class CategoryList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                category=Category(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['category'] = \
            [Category.from_element(child) for child in element.findall('category')]

        return a


class Class(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                title=Text(),
                category_list=CategoryList(),
                analyzed_list=AnalyzedList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('category_list')
        if child is not None:
            a['category_list'] = CategoryList.from_element(child)
        child = element.find('analyzed_list')
        if child is not None:
            a['analyzed_list'] = AnalyzedList.from_element(child)

        return a


class ClassList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                klass=Class(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['klass'] = \
            [Class.from_element(child) for child in element.findall('class')]

        return a


class Measure(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                title=Text(),
                units=Text(),
                param=Keyword(),
                class_list=ClassList(),
                dispersion=Keyword(),
                description=Text(),
                population=Text(),
                units_analyzed=Keyword(),
                analyzed_list=AnalyzedList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('units')
        if child is not None:
            a['units'] = child.text
        child = element.find('param')
        if child is not None:
            a['param'] = child.text
        child = element.find('class_list')
        if child is not None:
            a['class_list'] = ClassList.from_element(child)
        child = element.find('dispersion')
        if child is not None:
            a['dispersion'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text
        child = element.find('population')
        if child is not None:
            a['population'] = child.text
        child = element.find('units_analyzed')
        if child is not None:
            a['units_analyzed'] = child.text
        child = element.find('analyzed_list')
        if child is not None:
            a['analyzed_list'] = AnalyzedList.from_element(child)

        return a


class MeasureList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                measure=Measure(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['measure'] = \
            [Measure.from_element(child) for child in element.findall('measure')]

        return a


class OtherEvents(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                frequency_threshold=Float(),
                default_vocab=Keyword(),
                default_assessment=Keyword(),
                category_list=CategoryList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('frequency_threshold')
        if child is not None:
            a['frequency_threshold'] = child.text
        child = element.find('default_vocab')
        if child is not None:
            a['default_vocab'] = child.text
        child = element.find('default_assessment')
        if child is not None:
            a['default_assessment'] = child.text
        child = element.find('category_list')
        if child is not None:
            a['category_list'] = CategoryList.from_element(child)

        return a


class Outcome(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                type=Keyword(),
                title=Text(),
                description=Text(),
                time_frame=Text(),
                group_list=GroupList(),
                measure=Measure(),
                population=Text(),
                analysis_list=AnalysisList(),
                posting_date=Date(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('type')
        if child is not None:
            a['type'] = child.text
        child = element.find('title')
        if child is not None:
            a['title'] = child.text
        child = element.find('description')
        if child is not None:
            a['description'] = child.text
        child = element.find('time_frame')
        if child is not None:
            a['time_frame'] = child.text
        child = element.find('group_list')
        if child is not None:
            a['group_list'] = GroupList.from_element(child)
        child = element.find('measure')
        if child is not None:
            a['measure'] = Measure.from_element(child)
        child = element.find('population')
        if child is not None:
            a['population'] = child.text
        child = element.find('analysis_list')
        if child is not None:
            a['analysis_list'] = AnalysisList.from_element(child)
        child = element.find('posting_date')
        if child is not None:
            a['posting_date'] = child.text

        return a


class OutcomeList(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                outcome=Outcome(multi=True),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        a['outcome'] = \
            [Outcome.from_element(child) for child in element.findall('outcome')]

        return a


class ParticipantFlow(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_list=GroupList(),
                period_list=PeriodList(),
                recruitment_details=Text(),
                pre_assignment_details=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('group_list')
        if child is not None:
            a['group_list'] = GroupList.from_element(child)
        child = element.find('period_list')
        if child is not None:
            a['period_list'] = PeriodList.from_element(child)
        child = element.find('recruitment_details')
        if child is not None:
            a['recruitment_details'] = child.text
        child = element.find('pre_assignment_details')
        if child is not None:
            a['pre_assignment_details'] = child.text

        return a


class SeriousEvents(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                default_vocab=Keyword(),
                default_assessment=Keyword(),
                category_list=CategoryList(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('default_vocab')
        if child is not None:
            a['default_vocab'] = child.text
        child = element.find('default_assessment')
        if child is not None:
            a['default_assessment'] = child.text
        child = element.find('category_list')
        if child is not None:
            a['category_list'] = CategoryList.from_element(child)

        return a


class Baseline(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_list=GroupList(),
                analyzed_list=AnalyzedList(),
                measure_list=MeasureList(),
                population=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('group_list')
        if child is not None:
            a['group_list'] = GroupList.from_element(child)
        child = element.find('analyzed_list')
        if child is not None:
            a['analyzed_list'] = AnalyzedList.from_element(child)
        child = element.find('measure_list')
        if child is not None:
            a['measure_list'] = MeasureList.from_element(child)
        child = element.find('population')
        if child is not None:
            a['population'] = child.text

        return a


class ReportedEvents(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                group_list=GroupList(),
                serious_events=SeriousEvents(),
                other_events=OtherEvents(),
                time_frame=Text(),
                desc=Text(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('group_list')
        if child is not None:
            a['group_list'] = GroupList.from_element(child)
        child = element.find('serious_events')
        if child is not None:
            a['serious_events'] = SeriousEvents.from_element(child)
        child = element.find('other_events')
        if child is not None:
            a['other_events'] = OtherEvents.from_element(child)
        child = element.find('time_frame')
        if child is not None:
            a['time_frame'] = child.text
        child = element.find('desc')
        if child is not None:
            a['desc'] = child.text

        return a


class ClinicalResults(Nested):

    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
                participant_flow=ParticipantFlow(),
                baseline=Baseline(),
                outcome_list=OutcomeList(),
                reported_events=ReportedEvents(),
                certain_agreements=CertainAgreements(),
                point_of_contact=PointOfContact(),
                limitations_and_caveats=Keyword(),
            ),
            *args,
            **kwargs
        )

    @classmethod
    def from_element(cls, element):
        a = dict()

        child = element.find('participant_flow')
        if child is not None:
            a['participant_flow'] = ParticipantFlow.from_element(child)
        child = element.find('baseline')
        if child is not None:
            a['baseline'] = Baseline.from_element(child)
        child = element.find('outcome_list')
        if child is not None:
            a['outcome_list'] = OutcomeList.from_element(child)
        child = element.find('reported_events')
        if child is not None:
            a['reported_events'] = ReportedEvents.from_element(child)
        child = element.find('certain_agreements')
        if child is not None:
            a['certain_agreements'] = CertainAgreements.from_element(child)
        child = element.find('point_of_contact')
        if child is not None:
            a['point_of_contact'] = PointOfContact.from_element(child)
        child = element.find('limitations_and_caveats')
        if child is not None:
            a['limitations_and_caveats'] = child.text

        return a


class ClinicalStudy(DocType):

    rank = Integer()
    required_header = RequiredHeader()
    id_info = IdInfo()
    brief_title = Text()
    official_title = Text()
    sponsors = Sponsors()
    source = Text()
    brief_summary = BriefSummary()
    detailed_description = DetailedDescription()
    overall_status = Keyword()
    start_date = StartDate()
    completion_date = CompletionDate()
    phase = Keyword()
    study_type = Keyword()
    has_expanded_access = Keyword()
    study_design_info = StudyDesignInfo()
    enrollment = Enrollment()
    condition = Text(multi=True)
    intervention = Intervention(multi=True)
    eligibility = Eligibility()
    location = Location(multi=True)
    location_countries = LocationCountries()
    reference = Reference(multi=True)
    verification_date = Date()
    lastchanged_date = Date()
    firstreceived_date = Date()
    keyword = Text(multi=True)
    condition_browse = ConditionBrowse()
    intervention_browse = InterventionBrowse()
    primary_outcome = PrimaryOutcome(multi=True)
    overall_official = OverallOfficial(multi=True)
    link = Link(multi=True)
    responsible_party = ResponsibleParty()
    primary_completion_date = PrimaryCompletionDate()
    number_of_arms = Integer()
    arm_group = ArmGroup(multi=True)
    oversight_info = OversightInfo()
    secondary_outcome = SecondaryOutcome(multi=True)
    firstreceived_results_date = Date()
    clinical_results = ClinicalResults()
    last_known_status = Keyword()
    results_reference = ResultsReference(multi=True)
    acronym = Keyword()
    removed_countries = RemovedCountries()
    why_stopped = Text()
    number_of_groups = Integer()
    biospec_retention = Keyword()
    biospec_descr = BiospecDescr()
    patient_data = PatientData()
    firstreceived_results_disposition_date = Date()
    overall_contact = OverallContact()
    overall_contact_backup = OverallContactBackup()
    other_outcome = OtherOutcome(multi=True)
    study_docs = StudyDocs()
    target_duration = Keyword()
    expanded_access_info = ExpandedAccessInfo()

    @classmethod
    def from_element(cls, element):
        a = ClinicalStudy()

        a.rank = element.attrib.get('rank')
        child = element.find('required_header')
        if child is not None:
            a.required_header = RequiredHeader.from_element(child)
        child = element.find('id_info')
        if child is not None:
            a.id_info = IdInfo.from_element(child)
        child = element.find('brief_title')
        if child is not None:
            a.brief_title = child.text
        child = element.find('official_title')
        if child is not None:
            a.official_title = child.text
        child = element.find('sponsors')
        if child is not None:
            a.sponsors = Sponsors.from_element(child)
        child = element.find('source')
        if child is not None:
            a.source = child.text
        child = element.find('brief_summary')
        if child is not None:
            a.brief_summary = BriefSummary.from_element(child)
        child = element.find('detailed_description')
        if child is not None:
            a.detailed_description = DetailedDescription.from_element(child)
        child = element.find('overall_status')
        if child is not None:
            a.overall_status = child.text
        child = element.find('start_date')
        if child is not None:
            a.start_date = StartDate.from_element(child)
        child = element.find('completion_date')
        if child is not None:
            a.completion_date = CompletionDate.from_element(child)
        child = element.find('phase')
        if child is not None:
            a.phase = child.text
        child = element.find('study_type')
        if child is not None:
            a.study_type = child.text
        child = element.find('has_expanded_access')
        if child is not None:
            a.has_expanded_access = child.text
        child = element.find('study_design_info')
        if child is not None:
            a.study_design_info = StudyDesignInfo.from_element(child)
        child = element.find('enrollment')
        if child is not None:
            a.enrollment = Enrollment.from_element(child)
        a.condition = \
            [child.text for child in element.findall('condition')]
        a.intervention = \
            [Intervention.from_element(child) for child in element.findall('intervention')]
        child = element.find('eligibility')
        if child is not None:
            a.eligibility = Eligibility.from_element(child)
        a.location = \
            [Location.from_element(child) for child in element.findall('location')]
        child = element.find('location_countries')
        if child is not None:
            a.location_countries = LocationCountries.from_element(child)
        a.reference = \
            [Reference.from_element(child) for child in element.findall('reference')]
        child = element.find('verification_date')
        if child is not None:
            a.verification_date = child.text
        child = element.find('lastchanged_date')
        if child is not None:
            a.lastchanged_date = child.text
        child = element.find('firstreceived_date')
        if child is not None:
            a.firstreceived_date = child.text
        a.keyword = \
            [child.text for child in element.findall('keyword')]
        child = element.find('condition_browse')
        if child is not None:
            a.condition_browse = ConditionBrowse.from_element(child)
        child = element.find('intervention_browse')
        if child is not None:
            a.intervention_browse = InterventionBrowse.from_element(child)
        a.primary_outcome = \
            [PrimaryOutcome.from_element(child) for child in element.findall('primary_outcome')]
        a.overall_official = \
            [OverallOfficial.from_element(child) for child in element.findall('overall_official')]
        a.link = \
            [Link.from_element(child) for child in element.findall('link')]
        child = element.find('responsible_party')
        if child is not None:
            a.responsible_party = ResponsibleParty.from_element(child)
        child = element.find('primary_completion_date')
        if child is not None:
            a.primary_completion_date = PrimaryCompletionDate.from_element(child)
        child = element.find('number_of_arms')
        if child is not None:
            a.number_of_arms = child.text
        a.arm_group = \
            [ArmGroup.from_element(child) for child in element.findall('arm_group')]
        child = element.find('oversight_info')
        if child is not None:
            a.oversight_info = OversightInfo.from_element(child)
        a.secondary_outcome = \
            [SecondaryOutcome.from_element(child) for child in element.findall('secondary_outcome')]
        child = element.find('firstreceived_results_date')
        if child is not None:
            a.firstreceived_results_date = child.text
        child = element.find('clinical_results')
        if child is not None:
            a.clinical_results = ClinicalResults.from_element(child)
        child = element.find('last_known_status')
        if child is not None:
            a.last_known_status = child.text
        a.results_reference = \
            [ResultsReference.from_element(child) for child in element.findall('results_reference')]
        child = element.find('acronym')
        if child is not None:
            a.acronym = child.text
        child = element.find('removed_countries')
        if child is not None:
            a.removed_countries = RemovedCountries.from_element(child)
        child = element.find('why_stopped')
        if child is not None:
            a.why_stopped = child.text
        child = element.find('number_of_groups')
        if child is not None:
            a.number_of_groups = child.text
        child = element.find('biospec_retention')
        if child is not None:
            a.biospec_retention = child.text
        child = element.find('biospec_descr')
        if child is not None:
            a.biospec_descr = BiospecDescr.from_element(child)
        child = element.find('patient_data')
        if child is not None:
            a.patient_data = PatientData.from_element(child)
        child = element.find('firstreceived_results_disposition_date')
        if child is not None:
            a.firstreceived_results_disposition_date = child.text
        child = element.find('overall_contact')
        if child is not None:
            a.overall_contact = OverallContact.from_element(child)
        child = element.find('overall_contact_backup')
        if child is not None:
            a.overall_contact_backup = OverallContactBackup.from_element(child)
        a.other_outcome = \
            [OtherOutcome.from_element(child) for child in element.findall('other_outcome')]
        child = element.find('study_docs')
        if child is not None:
            a.study_docs = StudyDocs.from_element(child)
        child = element.find('target_duration')
        if child is not None:
            a.target_duration = child.text
        child = element.find('expanded_access_info')
        if child is not None:
            a.expanded_access_info = ExpandedAccessInfo.from_element(child)

        return a
