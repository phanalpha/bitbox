# -*- coding: utf-8 -*-

from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType
from elasticsearch.exceptions import RequestError
from xml.etree.ElementTree import parse
from glob import glob
import progressbar
import logging


def vivify(cls, index, idpath, files):
    cls.init(index=index)

    bar = progressbar.ProgressBar()
    for f in bar(files):
        try:
            document = parse(f)
            a = cls.from_element(document.getroot())
            a.save(id=document.find(idpath).text, index=index)
        except RequestError as e:
            logging.warning('<%s> %s' % (e, f))


def vivify_drive(args):
    connections.create_connection(hosts=args.hosts)
    names = args.name.split('.')
    mvars = vars(__import__(args.name))
    for name in names[1:]:
        mvars = vars(mvars[name])
    for cls in mvars.values():
        if cls is DocType:
            continue

        try:
            if issubclass(cls, DocType):
                vivify(
                    cls,
                    args.index or names[-1],
                    args.idpath,
                    glob(args.pathname)
                )
                break
        except TypeError as e:
            pass


def mount(subparsers):
    p = subparsers.add_parser('vivify')
    p.add_argument('name')
    p.add_argument('idpath')
    p.add_argument('pathname')
    p.add_argument('--hosts', nargs='*', default=['localhost'])
    p.add_argument('--index')
    p.set_defaults(func=vivify_drive)
