# -*- coding: utf-8 -*-

import argparse
from . import sculpt, vivify


def kick():
    parser = argparse.ArgumentParser(prog='bitbox')
    subparsers = parser.add_subparsers(title='Elasticsearch <- XML')
    for m in [sculpt, vivify]:
        m.mount(subparsers)
    args = parser.parse_args()

    if 'func' in vars(args):
        args.func(args)
    else:
        parser.print_help()
