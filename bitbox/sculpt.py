# -*- coding: utf-8 -*-

from collections import namedtuple, OrderedDict
from xml.etree.ElementTree import parse
from jinja2 import Environment
from glob import glob

import pendulum
import progressbar


def mapfield(f, p):
    fname = {'class': 'klass'}
    if f in fname:
        return fname[f]
    if f in ['name']:
        return p + '_' + f

    return f


def camelcase(underscore):
    return ''.join([s.capitalize() for s in underscore.split('_')])


def infer(candidates, n):
    try:
        for v in candidates:
            if v is None:
                continue
            int(v)
        return 'Integer'
    except ValueError:
        pass

    try:
        for v in candidates:
            if v is None:
                continue
            float(v)
        return 'Float'
    except ValueError:
        pass

    try:
        for v in candidates:
            if v is None:
                continue
            pendulum.parse(v)
        return 'Date'
    except (OverflowError, ValueError,
            pendulum.parsing.exceptions.ParserError):
        pass

    if len(candidates) / n < .03 or \
       all([len(c) < 33 for c in candidates if c]):  # threshold
        return 'Keyword'

    return 'Text'


def infer_type(k, f, n):
    return camelcase(k) if len(f.archetype.fields) > 0 else \
        infer(f.candidates, n)


class Archetype(namedtuple('Archetype', ['fields'])):

    _tmpl_str = """
{%- macro multi(f) %}
  {%- if f.count > 1 -%}
    multi=True
  {%- endif -%}
{% endmacro %}

{%- macro namef(k) %}
  {%- if root -%}
    a.{{k}}
  {%- else -%}
    a['{{k | mapfield(name)}}']
  {%- endif -%}
{% endmacro %}

{%- macro pick(k, f, v) %}
  {%- if f.archetype.fields | count > 0 -%}
    {{k | camelcase}}.from_element({{v}})
  {%- else -%}
    {{v}}.text
  {%- endif -%}
{% endmacro %}

class {{name | camelcase}}({{'DocType' if root else 'Nested'}}):
{% if root %}
  {%- for k, f in fields %}
    {{k | mapfield(name)}} = {{k | infer_type(f, n)}}({{multi(f)}})
  {%- endfor %}
{%- else %}
    def __init__(self, *args, **kwargs):
        super().__init__(
            properties=dict(
  {%- for k, f in fields %}
                {{k | mapfield(name)}}={{k | infer_type(f, n)}}({{multi(f)}}),
  {%- endfor %}
  {%- if literal %}
                inner_text={{candidates | infer(n)}}()
  {%- endif %}
            ),
            *args,
            **kwargs
        )
{%- endif %}

    @classmethod
    def from_element(cls, element):
        a = {% if root %}{{name | camelcase}}{% else %}dict{% endif %}()
{% for k, f in fields %}
  {%- if f.count < 1 %}
        {{namef(k)}} = element.attrib.get('{{k}}')
  {%- else %}
    {%- if f.count < 2 %}
        child = element.find('{{k}}')
        if child is not None:
            {{namef(k)}} = {{pick(k, f, 'child')}}
    {%- else %}
        {{namef(k)}} = \\
            [{{pick(k, f, 'child')}} for child in element.findall('{{k}}')]
    {%- endif %}
  {%- endif %}
{%- endfor %}
{%- if literal %}
        a['inner_text'] = element.text
{%- endif %}

        return a
"""

    @classmethod
    def sculpt(cls, element):
        fs = OrderedDict()
        for k, v in element.attrib.items():
            f = fs.get(k, Field())
            fs[k] = Field(candidates=f.candidates | {v})
        for child in element:
            f = fs.get(child.tag, Field())
            a = Archetype.sculpt(child)
            t = child.text and child.text.strip()
            fs[child.tag] = Field(
                f.archetype.coalesce(a),
                f.count + 1,
                (f.candidates | {t}) if t else f.candidates
            )

        return Archetype(fs)

    def __new__(cls, fields=OrderedDict(), *args, **kwargs):
        return super().__new__(cls, fields, *args, **kwargs)

    def __add__(self, other):
        if self.is_branch != other.is_branch:
            raise ValueError()

        return self.coalesce(other)

    def coalesce(self, other):
        fs = OrderedDict(self.fields)
        for k, f in other.fields.items():
            if k in fs:
                fs[k] = Field(
                    fs[k].archetype + f.archetype,
                    max(fs[k].count, f.count),
                    fs[k].candidates | f.candidates
                )
            else:
                fs[k] = f

        return Archetype(fs)

    @property
    def is_branch(self):
        for f in self.fields.values():
            if f.count > 0:
                return True

        return False

    def pprint(self, indent=4, depth=0):
        return ''.join([
            ' ' * indent * depth + f.name(k) + '\n' +
            f.archetype.pprint(indent, depth + 1)
            for k, f in self.fields.items()
        ])

    def compose(self, name, candidates, root=False, n=1):
        env = Environment()
        env.filters.update(
            camelcase=camelcase,
            mapfield=mapfield,
            infer_type=infer_type,
            infer=infer,
        )

        return env.from_string(self._tmpl_str).render(
            name=name,
            fields=self.fields.items(),
            literal=not self.is_branch and len(candidates) > 0,
            candidates=candidates,
            root=root,
            n=n
        )


class Field(namedtuple('Field', ['archetype', 'count', 'candidates'])):

    def __new__(cls, archetype=Archetype(), count=0, candidates=set()):
        return super().__new__(cls, archetype, count, candidates)

    def name(self, field_name):
        if self.count > 1:
            return field_name + '*'
        if self.count > 0:
            return field_name

        return '@' + field_name


def analyze(files):
    roots = OrderedDict()

    bar = progressbar.ProgressBar()
    for f in bar(files):
        root = parse(f).getroot()
        a = Archetype.sculpt(root)
        roots[root.tag] = roots.get(root.tag, Archetype()).coalesce(a)

    return roots


def squash(archetype, catalog):
    for k, f in archetype.fields.items():
        if len(f.archetype.fields) > 0:
            a, c = catalog.get(k, (Archetype(), set()))
            catalog[k] = (a.coalesce(f.archetype), c | f.candidates)
            squash(f.archetype, catalog)


def synthesize(roots):
    return '\n'.join([a.pprint() for a in roots.values()])


def synthesize_drive(args):
    print(synthesize(analyze(glob(args.pathname))))


def compose(roots, n):
    catalog = dict()
    for a in roots.values():
        squash(a, catalog)

    free_archetypes = []
    while len(free_archetypes) < len(catalog):
        for a in sorted(catalog):
            if a in free_archetypes:
                continue

            if all([k in free_archetypes
                    for k, f in catalog[a][0].fields.items()
                    if len(f.archetype.fields) > 0]):
                free_archetypes.append(a)

    return '\n'.join(
        ['# -*- coding: utf-8 -*-\n',
         'from elasticsearch_dsl import \\',
         '    DocType, Nested, Date, Float, Integer, Keyword, Text'] +
        [catalog[a][0].compose(a, catalog[a][1], False, n)
         for a in free_archetypes] +
        [archetype.compose(name, set(), True, n)
         for name, archetype in roots.items()]
    )


def compose_drive(args):
    print(compose(analyze(glob(args.pathname)), len(glob(args.pathname))))


def mount(subparsers):
    p = subparsers.add_parser('synthesize')
    p.add_argument('pathname')
    p.set_defaults(func=synthesize_drive)

    p = subparsers.add_parser('compose')
    p.add_argument('pathname')
    p.set_defaults(func=compose_drive)
