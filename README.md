BITBOX
======

A collection of tools to analyze bits (data).

USAGE
-----

A `bitbox` command comes with the package, which could be run with
`python -m` as well.

`synthesize` summarizes the structure of xml documents.

    bitbox synthesize 'Breast_Neoplasms-7682/*.xml'

`compose` generates the corresponding [Elasticsearch DSL](https://elasticsearch-dsl.readthedocs.io/) `DocType`.

    bitbox compose 'Breast_Neoplasms-7682/*.xml' > _generated/clinicaltrials.py

`vivify` indexes xml documents as the (generated) `DocType`.

    PYTHONPATH=. bitbox vivify _generated.clinicaltrials //id_info/nct_id 'Breast_Neoplasms-7682/*.xml'

[`index.mapping.nested_fields.limit`](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html) should be updated as necessary.

    curl -X PUT 'localhost:9200/clinicaltrials/_settings?preserve_existing&pretty' -d '{
        "index.mapping.nested_fields.limit": 200
    }'
