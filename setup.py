# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name='bitbox',
    version='0.1.2',
    packages=find_packages(exclude=['_generated']),
    entry_points=dict(
        console_scripts=[
            'bitbox = bitbox:kick'
        ],
    ),

    install_requires=[
        'Jinja2',
        'pendulum',
        'progressbar2',
        'elasticsearch-dsl',
    ]
)
